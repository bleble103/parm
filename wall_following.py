"""Sample Webots controller for the wall following benchmark."""
# author: Tomasz Markowski

from controller import Robot

#0 1 2 front
#15 14 13 rear


def getDistance(sensor):
    """
    Return the distance of an obstacle for a sensor.

    The value returned by the getValue() method of the distance sensors
    corresponds to a physical value (here we have a sonar, so it is the
    strength of the sonar ray). This function makes a conversion to a
    distance value in meters.
    """
    return ((1000 - sensor.getValue()) / 1000) * 5

def getSensorsDistance(sensors):
    minDist = 5
    for sensor in sensors:
        currDist = getDistance(sensor)
        if currDist < minDist:
            minDist = currDist
    return minDist

# Maximum speed for the velocity value of the wheels.
# Don't change this value.
MAX_SPEED = 5.24

# Get pointer to the robot.
robot = Robot()

# Get the time step of the current world.
timestep = int(robot.getBasicTimeStep())

# Get pointer to the robot wheels motors.
leftWheel = robot.getMotor('left wheel')
rightWheel = robot.getMotor('right wheel')

# We will use the velocity parameter of the wheels, so we need to
# set the target position to infinity.
leftWheel.setPosition(float('inf'))
rightWheel.setPosition(float('inf'))

# Get and enable the distance sensors.
frontSensor = robot.getDistanceSensor("so3")
frontSensor.enable(timestep)
sideSensor = robot.getDistanceSensor("so0")
sideSensor.enable(timestep)

robot.getDistanceSensor("so1").enable(timestep)
robot.getDistanceSensor("so2").enable(timestep)
robot.getDistanceSensor("so4").enable(timestep)
robot.getDistanceSensor("so5").enable(timestep)
robot.getDistanceSensor("so6").enable(timestep)

robot.getDistanceSensor("so15").enable(timestep)
robot.getDistanceSensor("so14").enable(timestep)
robot.getDistanceSensor("so13").enable(timestep)
robot.getDistanceSensor("so12").enable(timestep)
robot.getDistanceSensor("so11").enable(timestep)

frontSensors=[robot.getDistanceSensor("so0"),robot.getDistanceSensor("so1"),robot.getDistanceSensor("so2"),robot.getDistanceSensor("so3"),robot.getDistanceSensor("so4")]
rearSensors=[robot.getDistanceSensor("so15"),robot.getDistanceSensor("so14"),robot.getDistanceSensor("so13"),robot.getDistanceSensor("so12"),robot.getDistanceSensor("so11")]
topSensors=[robot.getDistanceSensor("so1"), robot.getDistanceSensor("so2"),robot.getDistanceSensor("so3"),robot.getDistanceSensor("so4"),robot.getDistanceSensor("so5"),robot.getDistanceSensor("so6")]

# Move forward until we are 50 cm away from the wall.
leftWheel.setVelocity(MAX_SPEED)
rightWheel.setVelocity(MAX_SPEED)
while robot.step(timestep) != -1:
    if getDistance(frontSensor) < 0.335:
        break

# Rotate clockwise until the wall is to our left.
leftWheel.setVelocity(MAX_SPEED)
rightWheel.setVelocity(-MAX_SPEED)
while robot.step(timestep) != -1:
    # Rotate until there is a wall to our left, and nothing in front of us.
    if getDistance(sideSensor) < 1:
        break

# Main loop.
while robot.step(timestep) != -1:

    # Too close to the wall, we need to turn right.
    frontMeas = getSensorsDistance(frontSensors)
    rearMeas = getSensorsDistance(rearSensors)
    
    delta = frontMeas - rearMeas
    aver = (frontMeas+rearMeas)/2
    intensity = 5
    
    coef = min(0.8, max(-0.8, intensity*delta))
    
    leftSpeed = MAX_SPEED
    rightSpeed = MAX_SPEED
    
    #if coef > 0:
    #    leftSpeed *= (1-coef)
    #elif coef < 0:
    #    rightSpeed *= (1+coef)
        
    
    delta = aver - 0.335
    intensity = 7
    coefa = min(0.6, max(-0.6, intensity*delta))
    
    if coefa > 0 and coef > 0:
        leftSpeed *= (1-coefa)
    elif coefa < 0 and coef < 0:
        rightSpeed *= (1+coefa)
        
    topDist = getSensorsDistance(topSensors)
        
    if frontMeas > 0.353:
        rightSpeed = MAX_SPEED
        leftSpeed = 0.5*MAX_SPEED
    elif topDist < 0.335:
        dt = min(1, 15*(0.5 - topDist))
        #rightSpeed = (1-dt)*MAX_SPEED
        rightSpeed = 0.1*MAX_SPEED
        leftSpeed = MAX_SPEED
        
    leftWheel.setVelocity(leftSpeed)
    rightWheel.setVelocity(rightSpeed)
        

# Stop the robot when we are done.
leftWheel.setVelocity(0)
rightWheel.setVelocity(0)
