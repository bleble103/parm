"""Sample Webots controller for the square path benchmark."""
# author: Tomasz Markowski

from controller import Robot
import math

# Get pointer to the robot.
robot = Robot()

# Get pointer to each wheel of our robot.
leftWheel = robot.getMotor('left wheel')
rightWheel = robot.getMotor('right wheel')


rightWheelSensor = robot.getPositionSensor('right wheel sensor')
rightWheelSensor.enable(16) # Refreshes the sensor every 16ms.

leftWheelSensor = robot.getPositionSensor('left wheel sensor')
leftWheelSensor.enable(16) # Refreshes the sensor every 16ms.

for i in range(4):
    leftWheel.setVelocity(5.24)
    rightWheel.setVelocity(5.24)


    leftWheel.setPosition(1000)
    rightWheel.setPosition(1000)

    val = leftWheelSensor.getValue()
    if math.isnan(val): val=0
    robot.step(1)
    print(val)
    while math.isnan(leftWheelSensor.getValue()) or leftWheelSensor.getValue() - val < 20.8:
        vel = min(5.24, 10*(20.8 - (leftWheelSensor.getValue() - val)))
        if vel < 0.2: break
    
        leftWheel.setVelocity(vel)
        rightWheel.setVelocity(vel)
        robot.step(1)
    
    if i == 3: break

    leftWheel.setPosition(1000)
    rightWheel.setPosition(-1000)

    leftWheel.setVelocity(2)
    rightWheel.setVelocity(2)
    if i != 1: robot.step(1250)
    else: robot.step(1240)

    
leftWheel.setVelocity(0)
rightWheel.setVelocity(0)
# Repeat the following 4 times (once for each side).

